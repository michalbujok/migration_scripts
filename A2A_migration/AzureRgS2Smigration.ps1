# with this script vm's can be moved to different subscription
# note that change of region is not possible
# by default this is "testing" move, since there is deny NSG created to new vm in new location
# Please update "parameters" 
# Login-AzAccount
# The Parameter $finalMigration defines if the migration is a test or a final migration to the target. In final migration, the source VM is powered off before snapshot and migration replication co ensure VM consistensy
# 
# Prerequisites for using the script for migration:
# Contributor access to in scope resource groups where VM's are in source subscription
# If no subscription level contributor access provided, the $SnapTempRG resourcegroup is required for creation of migration snapshots 
# contributor access to in scope subscription where vm's are migrated to.



param (
   [string]$sourcesubscription = "Metso-IT-Production",
   [string]$destinationsub = "Metso-IT-SH-Production",
   [string]$SnapTempRG = 'azSnapTemp',
   [switch]$finalMigration = $false,
   [switch]$createMigrRgs = $true,
   [string]$vmScopeCsv = "C:\temp\a2aVM.csv"

   )

##########

write-host ""`
"If you need to validate the subscription providers are in place in advance, please run commands:`n"`
"Set-ManagedDiskUpdateSubscription `$destinationsub`n"`
"Set-ManagedDiskUpdateSubscription `$sourcesubscription`n"`
"To run the migration with command AzRgMove`n"`
"You can provide alternative source, target subscription, resource group, and network information with parameters.`n"`
"By default, the AzRgMove will run migration validation, creating snapshots, replication vm to target and setting it in dedicated restircted NSG denying outbound traffic.`n"`
"This is important, as the test VM might run in parallel to the source VM and it might compromize the running configuration to run both in parallel`n"`
"To run a final replica without the NSG, to stop the source VM for consistent state, replicating the VM and starting it up run the AzRgMove -finalMigration `$true`n"`
"To let the scirpt create resource groups and availabilityset to target environment, set the parameter `$createMigrRgs  to `$true`n"`
"" -ForegroundColor Yellow
##########
#
#function for getting informatio about public IPs
#
function Get-PublicIpInfo {
    Param
    (
        [Parameter(Mandatory=$false, Position=0)]
        [string]$publicIpNicName,
        [Parameter(Mandatory=$false, Position=1)]
        [string]$rgName
    )
    $nicInfo = Get-AzNetworkInterface -ResourceGroupName $rgName -Name $publicIpNicName
    if ($nicInfo.IpConfigurations.PublicIpAddress.Id){
        $publicIpName = ($nicInfo.IpConfigurations.PublicIpAddress.Id -split ("publicIPAddresses/"))[1]
        $publicIpInfo = Get-AzPublicIpAddress -ResourceGroupName $rgName -Name $publicIpName | Select-Object Name, IpAddress, PublicIpAllocationMethod, Id, @{Name="SKU"; Expression = {$_.Sku.Name}}
        return $publicIpInfo    
    }
    else {
        return $null
    }
}

#
# Define function for setting Resource providers to realted subscriptions
#
function Set-ManagedDiskUpdateSubscription() {
    $ErrorActionPreference = 'Stop'
    Set-AzContext -SubscriptionName $args[0]
    Register-AzResourceProvider -ProviderNamespace Microsoft.Batch
    Register-AzProviderFeature -FeatureName ManagedResourcesMove -ProviderNamespace Microsoft.Compute
    $Job = Get-AzProviderFeature -FeatureName ManagedResourcesMove -ProviderNamespace Microsoft.Compute
    while (($Job.RegistrationState -ne 'Registered')) {
        write-host "current registation state:" $job.RegistrationState
        Write-host "sleeping 30 sec"
        start-Sleep 30;
        $Job = Get-AzProviderFeature -FeatureName ManagedResourcesMove -ProviderNamespace Microsoft.Compute
        }
    write-host "Registration ready"
    Register-AzResourceProvider -ProviderNamespace Microsoft.Compute 
    }
#
# Define function for VM resource move
#

function AzRgMove {

## Validate given target parameters
# 
    Param
    (
        [Parameter(Mandatory=$false, Position=0)]
        [string]$sourcesubscription = $sourcesubscription,
        [Parameter(Mandatory=$false, Position=1)]
        [string]$destinationsub = $destinationsub ,
        [Parameter(Mandatory=$false, Position=2)]
        [string]$SnapTempRG = $SnapTempRG ,
        [Parameter(Mandatory=$false, Position=3)]
        [switch] $createMigrRgs = $createMigrRgs,
        [Parameter(Mandatory=$false, Position=4)]
        [switch] $finalMigration = $finalMigration,
        [Parameter(Mandatory=$false, Position=5)]
        [string] $vmScopeCsv = $vmScopeCsv

    )

$startTime = (Get-Date)
$initialGrid = Import-Csv -Path $vmScopeCsv
Write-Host "Select server to migrate" -ForegroundColor Yellow
$vmsInScope = $initialGrid | Out-GridView -Title "Select server to migrate" -PassThru
$vmsInScopeStr = $vmsInScope | Format-Table | Out-String
Write-Host $vmsInScopeStr -ForegroundColor Green

$subdetails = Get-AzSubscription -SubscriptionName $sourcesubscription
if ( $subdetails.Name -ne $sourcesubscription) {
    write-host "Check the Source Subscription  $sourcesubscription is valid" -ForegroundColor Red
    break
    }
$subdetails = Get-AzSubscription -SubscriptionName $destinationsub
if ( $subdetails.Name -ne $destinationsub) {
    write-host "Check the Source Subscription  $destinationsub is valid" -ForegroundColor Red
    break
    }

Select-AzSubscription -SubscriptionName $sourcesubscription
#
## Valiate source subscription resources
#
$rgValidate=""
$rgValidate=Get-AzResourceGroup -Name $SnapTempRG
if ( $rgValidate.ResourceGroupName -ne $SnapTempRG) {
    if ($createMigrRgs -eq $true ) {
         Write-Host "Resourcegroup for snapshots missing in source subscription, as  parameter createMigrRgs is set to $true, creating new RG" 
         New-AzResourceGroup -Name $SnapTempRG -Location $destinationlocation
         #if ( $? -ne 0 ) {  # wrong return code
         #   Write-Host "Problem creating new resource group $SnapTempRG in subscription $sourcesubscription"
         #   break
         #   }
         }
         elseif($createMigrRgs -eq $false ) {
            Write-Host "Resourcegroup for snapshots missing in source subscription. Parameter createMigrRgs is set to $false"
            Write-Host "Please validate the Resource Group name, source environment, create new Resource group or enable Resource Group creation by changing the parameter createMigrRgs value to $true! " 
            break
            }
    }

# 
# Set Resource providers to related subscriptions 
# 
Set-ManagedDiskUpdateSubscription $destinationsub
Set-ManagedDiskUpdateSubscription $sourcesubscription

ForEach ($VM in $vmsInScope) {

    $SourceRG = $vm.SourceRG
    $DestinationRG = $vm.DestinationRG
    $VNetRgName = $vm.DestinationVnetRg
    $VnetName = $vm.DestinationVnet
    $SubnetName = $vm.DestinationSubnet
    $movePubIp = $vm.MovePubIp
    $sourcelocation = $vm.Location
    $destinationlocation = $vm.Location


    # Print parameters:
    Write-Host "`
    sourcesubscription:    $sourcesubscription `
    destinationsub:        $destinationsub `
    SourceRG:              $SourceRG `
    virtualMachineName:    "$VM.Name"`
    SnapTempRG:            $SnapTempRG`
    DestinationRG:         $DestinationRG`
    destinationlocation:   $destinationlocation `
    sourcelocation:        $sourcelocation`
    VNetName:              $VNetName `
    SubnetName:            $SubnetName`
    vnetRgName:            $vnetRgName`
    PreservePubIP:         $movePubIp`
    finalMigration:        $finalMigration`
    createMigrRgs:         $createMigrRgs`
    " -ForegroundColor Yellow

    Select-AzSubscription -SubscriptionName $destinationsub

    $vnetValidate=Get-AzVirtualNetwork -ResourceGroupName $VNetRgName -Name $VNetName
    $vnetValidateSubnet=$vnetValidate | Get-AzVirtualNetworkSubnetConfig -Name $subnetname
    if ( $vnetValidate.Name -ne $VNetName ) {
        Write-host "Target Virtual Network: $VNetName do not exist, please validate the paramters or target environment!!" -ForegroundColor Red
        break
    } 
    if ($vnetValidateSubnet.Name -ne $subnetname ) {
        Write-host "Target  Subnet: $subnetname do not exist, please validate the paramters or target environment!!" -ForegroundColor Red
        break
    } 

    $rgValidate=Get-AzResourceGroup -Name $DestinationRG
    if ( $rgValidate.ResourceGroupName -ne $DestinationRG) {
        if ($createMigrRgs -eq $true ) {
            Write-Host "Destination ResourceGroup missing, as  parameter createMigrRgs is set to $true, creating new RG, please validate target resource group policies, rights and tags" 
            Write-host "Executing: New-AzResourceGroup -Name $DestinationRG -Location $destinationlocation"
            New-AzResourceGroup -Name $DestinationRG -Location $destinationlocation
            $Job = Get-AzResourceGroup -Name $DestinationRG
            while (($Job.ProvisioningState -ne 'Succeeded')) {
                write-host "current registation state:" $job.ProvisioningState
                Write-host "sleeping 30 sec"
                start-Sleep 30;
                $Job = Get-AzResourceGroup -Name $DestinationRG
            }
            write-host "Resourcegroup $DestinationRG ready"
        }
        elseif($createMigrRgs -eq $false ) {
            Write-Host "Destination ResourceGroup missing. Parameter createMigrRgs is set to $false"
            Write-Host "Please validate the Resource Group name, target environment, create new Resource group or enable Resource Group creation by changing the parameter createMigrRgs value to $true! " 
            break
        }
    }

    Select-AzSubscription -SubscriptionName $sourcesubscription
    #
    # Get info about public IP addresses and Interfaces connected to VM 
    #
    $pubNicInfo = @()
    $vmInfo = Get-AzVM -ResourceGroupName $SourceRG -Name $vm.Name
    $networkInterfaces = $vmInfo.NetworkProfile.NetworkInterfaces
    foreach ($nicId in $networkInterfaces) {
        $nicName = ($nicId.Id -split ("networkInterfaces/"))[1]
        $nicOrder = $nicId.Primary
        $nicAcceleratedNetworking = (Get-AzNetworkInterface -ResourceGroupName $SourceRG -Name $nicName).EnableAcceleratedNetworking
        $publicIpInfo = Get-PublicIpInfo -publicIpNicName $nicName -rgName $sourceRg
        $out = Get-AzVM -ResourceGroupName $sourceRg -VMName $vm.Name | Select-Object Name, @{Name="NicName"; Expression = {$nicName}}, @{Name="PrimaryNic"; Expression = {$nicOrder}}, @{Name="AcceleratedNetworking"; Expression = {$nicAcceleratedNetworking}}, @{Name="PublicIpName"; Expression = {$publicIpInfo.Name}}, @{Name="PublicIpAddress"; Expression = {$publicIpInfo.IpAddress}}, @{Name="PublicIpSKU"; Expression = {$publicIpInfo.SKU}}
        $pubNicInfo += $out
        $publicIpInfo = "" 
    }
    
$pubNicInfo

    $virtualMachineName=($VM).Name

    #get some vm details
    $vmdetails = Get-AzVM -Name $virtualMachineName -ResourceGroupName $SourceRG
    $nicdetails = Get-AzResource -ResourceId $vmdetails.NetworkProfile.NetworkInterfaces.id
    $vmdetails.OSProfile

    $availabilitySetConfig=(Get-AzResource -ResourceId ($vmdetails.AvailabilitySetReference).Id -ErrorAction SilentlyContinue)

    Select-AzSubscription -SubscriptionName $destinationsub
    #
    # Validate AvailabilitySet and create if missing and allowed
    #
    $asValidate=""
    $availSet=""
    $asValidate=Get-AzAvailabilitySet -ResourceGroupName $DestinationRG -Name $availabilitySetConfig.Name -ErrorAction SilentlyContinue
    if ( $asValidate.Name -ne $availabilitySetConfig.Name) {
        if ($createMigrRgs -eq $true ) {
            Write-Host "Destination availabilityset missing, as  parameter createMigrRgs is set to $true, creating new availabilityset" -ForegroundColor Yellow
            $availSet = New-AzAvailabilitySet -Location $destinationlocation -Name $availabilitySetConfig.Name -ResourceGroupName $DestinationRG -PlatformUpdateDomainCount $availabilitySetConfig.Properties.platformUpdateDomainCount -PlatformFaultDomainCount $availabilitySetConfig.Properties.platformFaultDomainCount -Sku $availabilitySetConfig.Sku.name
        }
        elseif($createMigrRgs -eq $false ) {
            Write-Host "Destination availabilityset missing. Parameter createMigrRgs is set to $false" -ForegroundColor Red
            Write-Host "Please validate the AvailabilitySet name, Resource Group name, target environment, create new AvailabilitySet or enable creation by changing the parameter createMigrRgs value to $true! " -ForegroundColor Red
            break
        }
    }

    Select-AzSubscription -SubscriptionName $sourcesubscription

        if( $finalMigration -eq $true ){
            write-host "Final migration requested. Detaching public IP if needed and powering off the source VM before shapshot and migration!!" -ForegroundColor Yellow
            #
            # Verify if Public IP can be migrated and Disconnect and migrate public IP
            #
            if ($movePubIp -eq "True") {
                $i = @()
                foreach ($i in $pubNicInfo) {
                    if ($i.PublicIpSku -like "Basic") {
                    Write-Host ("Ip address:", $i.PublicIpName, "can be migrated.") -ForegroundColor Green
                    Write-Host ("Setting allocation to static. ") -ForegroundColor Yellow
                    $publicIp = Get-AzPublicIpAddress -ResourceGroupName $SourceRG -Name $i.PublicIpName
                    $publicIp.PublicIpAllocationMethod = "Static"
                    Set-AzPublicIpAddress -PublicIpAddress $publicIp
                    Stop-AzVM -name $VM.Name -ResourceGroupName $SourceRG -force
                    New-AzResourceLock -LockLevel ReadOnly -LockName "migrationBlockVM" -ResourceGroupName $SourceRG -ResourceName $virtualMachineName -ResourceType "Microsoft.Compute/virtualMachines" -Force
                    Write-Host ("Detaching public ip of VM", $i.Name, " from resource.") -ForegroundColor Yellow
                    $nicPubIp = Get-AzNetworkInterface -Name $i.Nicname -ResourceGroupName $sourceRg
                    $nicPubIp.IpConfigurations.PublicIpAddress.Id = $null
                    Set-AzNetworkInterface -NetworkInterface $nicPubIp
                    Write-Host ("Moving public IP", $i.PublicIpName, " to destination subscription.") -ForegroundColor Yellow
                    $MoveResource2 = Get-AzResource -ResourceType "Microsoft.Network/publicIPAddresses" -ResourceGroupName $SourceRG -ResourceName $i.PublicIpName
                    Move-AzResource -ResourceId $MoveResource2.ResourceId -DestinationSubscriptionId $subdetails.Id -DestinationResourceGroupName $DestinationRG -Force
                    }
                    elseif (($i.PublicIpName) -and ($i.PublicIpSku -notlike "Basic")) {
                        Write-Host ("Ip address:", $i.PublicIpName, "cannot be migrated. SKU is", $iPublicIpSku, ".") -ForegroundColor Red
                        Stop-AzVM -name $VM.Name -ResourceGroupName $SourceRG -force
                        New-AzResourceLock -LockLevel ReadOnly -LockName "migrationBlockVM" -ResourceGroupName $SourceRG -ResourceName $virtualMachineName -ResourceType "Microsoft.Compute/virtualMachines" -Force
                    }
                    elseif ((!$i.PublicIpName) -and (!$i.PublicIpAddress) -and (!$i.PublicIpSKU)) {
                        Write-Host ("Public Ip is not assigned for interface", $i.NicName, "of VM", $i.Name) -ForegroundColor Red
                        Stop-AzVM -name $VM.Name -ResourceGroupName $SourceRG -force
                        New-AzResourceLock -LockLevel ReadOnly -LockName "migrationBlockVM" -ResourceGroupName $SourceRG -ResourceName $virtualMachineName -ResourceType "Microsoft.Compute/virtualMachines" -Force
                    }
                }                
            }       
            else {
                Write-Host ("Public IP will not be migrated for VM", $vm.Name) -ForegroundColor Yellow
                Stop-AzVM -name $VM.Name -ResourceGroupName $SourceRG -force
                New-AzResourceLock -LockLevel ReadOnly -LockName "migrationBlockVM" -ResourceGroupName $SourceRG -ResourceName $virtualMachineName -ResourceType "Microsoft.Compute/virtualMachines" -Force
            }         
    }
    # make OS Disk snapshot
    $vmOSDisks = (get-Azvm -ResourceGroupName $SourceRG -Name $virtualMachineName).StorageProfile.OsDisk.Name
    $vmOSDisksType = (get-Azvm -ResourceGroupName $SourceRG -Name $virtualMachineName).StorageProfile.OsDisk.OsType
    $osdisk = Get-AzDisk -ResourceGroupName $SourceRG -DiskName $vmOSDisks
    if ($osdisk.Sku.Name -like "*Standard*") {
        $snapshotOS = New-AzSnapshotConfig -SourceUri $osdisk.Id -CreateOption Copy -Location $sourcelocation -SkuName "Standard_LRS"
    }
    else {
        $snapshotOS = New-AzSnapshotConfig -SourceUri $osdisk.Id -CreateOption Copy -Location $sourcelocation -SkuName "Premium_LRS"
    }
    $snapshotNameOS = ($vmOSDisks.ToLower()+'_snap')

    New-AzSnapshot -Snapshot $snapshotOS -SnapshotName $snapshotNameOS -ResourceGroupName $SnapTempRG
    #
    # make Data Disk snapshots
    #
    $vmDataDisks = (get-Azvm -ResourceGroupName $SourceRG -Name $virtualMachineName).StorageProfile.DataDisks.Name
    $dataDiskTable = @()

    ForEach ($vmDataDisk in $vmDataDisks) {
        $datadisk = Get-AzDisk -ResourceGroupName $SourceRG -DiskName $vmDataDisk
        $dataDiskStorageProfile = $vmdetails.StorageProfile.DataDisks | Where-Object {$_.Name -eq $vmDataDisk}
        if ($datadisk.Sku.Name -like "*Standard*") {
            $snapshotDATA = New-AzSnapshotConfig -SourceUri $datadisk.Id -CreateOption Copy -Location $destinationlocation -SkuName "Standard_LRS"
        }
        else {
            $snapshotDATA = New-AzSnapshotConfig -SourceUri $datadisk.Id -CreateOption Copy -Location $destinationlocation -SkuName "Premium_LRS"
        }
        $snapshotNameDATA = ($vmdatadisk.ToLower()+'_snap') 
        New-AzSnapshot -Snapshot $snapshotDATA -SnapshotName $snapshotNameDATA -ResourceGroupName $SnapTempRG
        $dataDiskTempTable = $datadisk | Select-Object Name, @{Name="SKU"; Expression = {$_.Sku.Name}}, @{Name="Caching"; Expression = {$dataDiskStorageProfile.Caching}}
        $dataDiskTable += $dataDiskTempTable
        }
    # Get Details from destination subscription
    $subdetails = Get-AzSubscription -SubscriptionName $destinationsub
    #move os disk
    $MoveResource = Get-AzResource -ResourceType "Microsoft.Compute/snapshots" -ResourceName $snapshotNameOS -ResourceGroupName $SnapTempRG
    Move-AzResource -ResourceId $MoveResource.ResourceId -DestinationSubscriptionId $subdetails.Id -DestinationResourceGroupName $DestinationRG -Force
    #
    #move data disks
    #
    ForEach ($vmDataDisk in $vmDataDisks) {
        $snapshotNameDATA = ($vmdatadisk.ToLower()+'_snap') 
        $MoveResource2 = Get-AzResource -ResourceType "Microsoft.Compute/snapshots" -ResourceName $snapshotNameDATA  -ResourceGroupName $SnapTempRG
        Move-AzResource -ResourceId $MoveResource2.ResourceId -DestinationSubscriptionId $subdetails.Id -DestinationResourceGroupName $DestinationRG -Force
    }
    # Start Creation of new VM
    Set-AzContext -SubscriptionName $destinationsub
    #
    # Check if target resourcegroup exists, if not create the RG
    #
    # 
    #
    # Create temp NSG blocking all outbound trafic for migrated VM
    #
    $nsgRule = New-AzNetworkSecurityRuleConfig `
    -Name 'DenyAllOutbound' `
    -Protocol Tcp `
    -Direction Outbound `
    -Priority 1000 `
    -SourceAddressPrefix * `
    -SourcePortRange * `
    -DestinationAddressPrefix * `
    -DestinationPortRange * `
    -Access Deny
    # Crete NSG
    $nsgName = $virtualMachineName + '-nsg'
    $nsg = New-AzNetworkSecurityGroup `
    -ResourceGroupName $DestinationRG `
    -Location $destinationlocation `
    -Name $nsgName `
    -SecurityRules $nsgRule
    #
    #Create NIC
    #
    $existingNic = Get-AzNetworkInterface -ResourceGroupName $DestinationRG | Where-Object {(($_.VirtualMachine -eq $null) -and ($_.Name -like "$virtualMachineName*"))}
    if ($existingNic) {
        $existingNicVnet = ((($existingNic.IpConfigurations[0].Subnet.Id -split ("virtualNetworks/"))[1]) -split ("/subnets"))[0]
        $existingNicSubnet = ($existingNic.IpConfigurations[0].Subnet.Id -split ("subnets/"))[1]
    }
    $nicname = $nicdetails.Name 
    $VNet = Get-AzVirtualNetwork -Name $VNetName -ResourceGroupName $vnetrgName
    $Subnet = $VNet.Subnets | Where-Object { $_.Name -eq $SubnetName }
    if (($existingNic) -and ($existingNicVnet -eq $VnetName) -and ($existingNicSubnet -eq $subnetName)){
        $nic = $existingNic
        if ( $finalMigration -eq $false ){
            $nic.NetworkSecurityGroup = $nsg
            $nic | Set-AzNetworkInterface
        }
        elseif ( $finalMigration -eq $true){
            $nic.NetworkSecurityGroup = $null
            $nic | Set-AzNetworkInterface
        }
    }
    elseif (!$existingNic){    
        if( $finalMigration -eq $false ){
            if ($pubNicInfo.AcceleratedNetworking -eq "True") {
                $nic = New-AzNetworkInterface -Location $destinationlocation -Name $NICName -ResourceGroupName $DestinationRG -Subnet $Subnet -NetworkSecurityGroup $nsg -EnableAcceleratedNetworking
            }
            else {
                $nic = New-AzNetworkInterface -Location $destinationlocation -Name $NICName -ResourceGroupName $DestinationRG -Subnet $Subnet -NetworkSecurityGroup $nsg
            }
        }
        elseif( $finalMigration -eq $true ){
            if ($pubNicInfo.AcceleratedNetworking -eq "True") {
                $nic = New-AzNetworkInterface -Location $destinationlocation -Name $NICName -ResourceGroupName $DestinationRG -Subnet $Subnet -EnableAcceleratedNetworking
            }
            else {
                $nic = New-AzNetworkInterface -Location $destinationlocation -Name $NICName -ResourceGroupName $DestinationRG -Subnet $Subnet
            }
        }
    }
    # Create OS Disk from SNAP
    $snapshot = Get-AzSnapshot -ResourceGroupName $DestinationRG -SnapshotName $snapshotNameOS 
    $diskConfig = New-AzDiskConfig -AccountType $osdisk.Sku.Name -Location $destinationlocation -SourceResourceId $snapshot.Id -CreateOption Copy
    $disk = New-AzDisk -ResourceGroupName $DestinationRG -DiskName $vmOSDisks -Disk $diskConfig 
    # Create Data Disks from SNAP
    ForEach ($vmDataDisk in $vmDataDisks) {
        $dataDiskSku = $dataDiskTable | Where-Object {$_.Name -eq $vmDataDisk}
        $snapshotNamedatadisk = ($vmDataDisk.ToLower() + '_snap')
        $snapshotdiskdata = Get-AzSnapshot -ResourceGroupName $DestinationRG -SnapshotName $snapshotNamedatadisk
        $diskConfig_data = New-AzDiskConfig -AccountType $dataDiskSku.SKU -Location $destinationlocation -SourceResourceId $snapshotdiskdata.Id -CreateOption Copy 
        $diskdata = New-AzDisk -ResourceGroupName $DestinationRG -DiskName $vmDataDisk -Disk $diskConfig_data
    }
    #Initialize virtual machine configuration
    $command='New-AzVMConfig -VMName $virtualMachineName -VMSize $vmdetails.HardwareProfile.VmSize '
    if( $vmdetails.Tags ) {
        $command=$command +' -Tags $vmdetails.Tags'
    }
    if( $vmdetails.Zones ) {
        $command=$command +' -Zone $vmdetails.Zones'
    }
    if(  $vmdetails.LicenseType ) {
        $command=$command+' -LicenseType $vmdetails.LicenseType'
    }
    if(    $availSet ) {
        $command=$command+' -AvailabilitySetId  $availSet.id'
    }
    write-host $command
    # Invoke-Expression $command 
    $VirtualMachine = Invoke-Expression $command
    if($vmOSDisksType -eq "Linux") {
        $VirtualMachine = Set-AzVMOSDisk -VM $VirtualMachine -ManagedDiskId $disk.Id -CreateOption Attach -Linux
    }
    elseif ($vmOSDisksType -eq "Windows") {
        $VirtualMachine = Set-AzVMOSDisk -VM $VirtualMachine -ManagedDiskId $disk.Id -CreateOption Attach -Windows
    }
    $lun = 0
    ForEach ($vmDataDisk in $vmDataDisks) {
        $dataDiskCache = $dataDiskTable | Where-Object {$_.Name -eq $vmDataDisk}
        $datadiskdetails = Get-AzDisk -DiskName $vmDataDisk -ResourceGroupName $DestinationRG
        $VirtualMachine = Add-AzVMDataDisk -VM $VirtualMachine -ManagedDiskId $datadiskdetails.Id -Name $vmDataDisk -CreateOption Attach -lun $lun -Caching $dataDiskCache.Caching
        $lun = $lun + 1
    }
    $VirtualMachine = Add-AzVMNetworkInterface -VM $VirtualMachine -Id $nic.Id
    write-host "executing: New-AzVM -VM $VirtualMachine -ResourceGroupName $DestinationRG -Location $destinationlocation" -ForegroundColor Yellow
    New-AzVM -VM $VirtualMachine -ResourceGroupName $DestinationRG -Location $destinationlocation
    #
    # Attach public IP
    if (($movePubIp -eq "True") -and ($finalMigration -eq $true) -and ($pubNicInfo.PublicIpName)) {
        $pip = Get-AzPublicIpAddress -ResourceGroupName $DestinationRg -Name $pubNicInfo.PublicIpName
        $newVM = Get-AzVM -ResourceGroupName $DestinationRG  -Name $virtualMachineName
        $newVMNicName = ($newVM.NetworkProfile.NetworkInterfaces.Id -split ("networkInterfaces/"))[1]
        $newVMNicConfig = Get-AzNetworkInterface -ResourceGroupName $DestinationRG -Name $newVMNicName
        $newVMNicConfig.IpConfigurations[0].PublicIpAddress = $pip
        Set-AzNetworkInterface -NetworkInterface $newVMNicConfig
    }
    $endTime = (Get-Date)
    $ElapsedTime = ($endTime-$startTime)
    ($ElapsedTime).TotalSeconds
    }
}