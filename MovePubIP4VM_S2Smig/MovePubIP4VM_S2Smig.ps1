$sourceRg = "mbujok-sourceA2A-rg"
$outputContent = @()
$virtualMachines = Get-AzVM -ResourceGroupName $sourceRg

function Get-PublicIpInfo {
    Param
    (
        [Parameter(Mandatory=$false, Position=0)]
        [string]$publicIpNicName,
        [Parameter(Mandatory=$false, Position=1)]
        [string]$rgName
    )
    $nicInfo = Get-AzNetworkInterface -ResourceGroupName $rgName -Name $publicIpNicName
    if ($nicInfo.IpConfigurations.PublicIpAddress.Id){
        $publicIpName = ($nicInfo.IpConfigurations.PublicIpAddress.Id -split ("publicIPAddresses/"))[1]
        $publicIpInfo = Get-AzPublicIpAddress -ResourceGroupName $rgName -Name $publicIpName | Select-Object Name, IpAddress, PublicIpAllocationMethod, Id, @{Name="SKU"; Expression = {$_.Sku.Name}}
        return $publicIpInfo    
    }
    else {
        return $null
    }
}

foreach ($vm in $virtualMachines) {
    $networkInterfaces = $vm.NetworkProfile.NetworkInterfaces
    foreach ($nicId in $networkInterfaces) {
        $nicName = ($nicId.Id -split ("networkInterfaces/"))[1]
        $nicOrder = $nicId.Primary
        $publicIpInfo = Get-PublicIpInfo -publicIpNicName $nicName -rgName $sourceRg
        $out = Get-AzVM -ResourceGroupName $sourceRg -VMName $vm.Name | Select-Object Name, @{Name="NicName"; Expression = {$nicName}}, @{Name="PrimaryNic"; Expression = {$nicOrder}}, @{Name="PublicIpName"; Expression = {$publicIpInfo.Name}}, @{Name="PublicIpAddress"; Expression = {$publicIpInfo.IpAddress}}, @{Name="PublicIpSKU"; Expression = {$publicIpInfo.SKU}}
        $outputContent += $out
        $publicIpInfo = "" 
    }
}
$outputContent | Format-Table
#$outputContent | Export-Csv -Path C:/temp/IPsExport.csv -NoTypeInformation -Encoding UTF8

# foreach ($vm in $outputContent) {
#    if ($vm.PublicIpSku -like "Basic") {
#        Write-Host ("Ip address:", $vm.Name, "can be migrated.") -ForegroundColor Green
#        Write-Host ("Detaching public ip", $vm.Name, " from resource.") -ForegroundColor Yellow
#        $nic = Get-AzNetworkInterface -Name $vm.Nicname -ResourceGroupName $sourceRg
#        $nic.IpConfigurations.PublicIpAddress.Id = $null
#        Set-AzNetworkInterface -NetworkInterface $nic               
#    }
#    elseif (($vm.PublicIpName) -and ($vm.PublicIpSku -notlike "Basic")) {
#        Write-Host ("Ip address:", $vm.PublicIpName, "cannot be migrated. SKU is", $vm.PublicIpSku, ".") -ForegroundColor Red
#         }
#    elseif ((!$vm.PublicIpName) -and (!$vm.PublicIpAddress) -and (!$vm.PublicIpSKU)) {
#        Write-Host ("Public Ip is not assigned for interface", $vm.NicName, "of VM", $vm.Name) -ForegroundColor Red
#    }
#}